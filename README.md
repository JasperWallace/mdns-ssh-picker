
Discover devices running ssh via mdns/Bounjour and pick one to connect to.

Avahi has a GUI utility to do it (bssh), but it doesn't merge records with the same
name - with both Ethernet and WIFI connected and ipv4 and ipv6 you get 4
entries in the list for each host!

This tool merges the names on the list.

TODO:

 * command line parsing
  * https://github.com/kbknapp/clap-rs/ (?)
  * lots of comments here: https://www.reddit.com/r/rust/comments/36yfbf/pirate_a_commandline_arrrrguments_parser/
  * https://github.com/tailhook/rust-argparse
  

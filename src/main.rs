extern crate multicast_dns;
use std::collections::HashMap;
use std::io;
use std::process::Command;
use std::sync::Mutex;
use std::env;

use multicast_dns::discovery::discovery_manager::*;

// exit() exits without calling destructors
// since multicast_dns wraps avahi we want to make sure the destructors get called
// so things can be free'd etc...
fn main() {
    let exit_code = real_main();
    std::process::exit(exit_code);
}

fn real_main() -> i32 {
    let discovery_manager = DiscoveryManager::new();

    let found: Mutex<Vec<ServiceInfo>> = Mutex::new(Vec::new());
    // grab the command line args
    let args: Vec<String> = env::args().collect();
    let mut service_type = "_ssh._tcp";

    if args.len() > 1 && args[1].starts_with("_") {
        service_type = &args[1];
    }

    let on_service_resolved = |service: ServiceInfo| {
        // we have to do this to get data out of the closure.
        found.lock().unwrap().push(service);
    };

    let on_service_discovered = |service: ServiceInfo| {

        let resolve_listeners = ResolveListeners {
            on_service_resolved: Some(&on_service_resolved),
        };

        discovery_manager.resolve_service(service, resolve_listeners);
    };

    let on_all_discovered = || {
        discovery_manager.stop_service_discovery();
    };

    let discovery_listeners = DiscoveryListeners {
        on_service_discovered: Some(&on_service_discovered),
        on_all_discovered: Some(&on_all_discovered),
    };

    discovery_manager.discover_services(&service_type, discovery_listeners);

    // did we find anything?
    if found.lock().unwrap().len() == 0 {
        println!("Didn't find any services while looking for {}.", service_type);
        return -1;
    }

    let fb = found.lock().unwrap();
    let mut results: HashMap<&str, Vec<&ServiceInfo>> = HashMap::new();

    for idx in 0..fb.len() {
        let ref service = fb[idx];

        //        println!("found ssh host: {}.{} : {}", service.name.as_ref().unwrap(),
        //                    service.domain.as_ref().unwrap(), service.address.as_ref().unwrap());
        // we only really want hosts on the local network segment
        if service.domain.as_ref().unwrap() == "local" {
            results.entry(service.host_name.as_ref().unwrap()).or_insert(Vec::new()).push(service);
        }
    }


    // sort the keys
    let mut ks = Vec::new();
    for name in results.keys() {
        ks.push(name);
    }
    ks.sort();

    let mut choice: u32 = 1; // start at 1 for convience to humans

    for name in &ks {
        let empty_string = "".to_owned();
        println!("{}: {}.", choice, name);
        let services = match results.get(*name) {
            Some(thing) => thing,
            None => panic!()
        };
        for s in services {
            println!("\t{} {:?}: {} {} {}",
                     s.interface,
                     s.protocol,
                     s.address.as_ref().unwrap(),
                     s.name.as_ref().unwrap(),
                     s.txt
                      .as_ref()
                      .unwrap_or(&empty_string));
        }
        println!("");
        choice += 1;
    }

    println!("Please enter the number of the host to connect to (1-{}): ",
             choice - 1);

    let mut chosen = String::new();
    io::stdin()
        .read_line(&mut chosen)
        .ok()
        .expect("Failed to read line");

    let chosen: u32 = chosen.trim().parse().expect("That's not a number!");

    let cmd = match service_type {
        "_ssh._tcp" => "ssh",
        "_http._tcp" => "x-www-browser", // very debian :( not so portable
        _ => "echo" // just echo the args
    };
    let mut cmd = Command::new(cmd);
    let mut chose: bool = false;

    // we skip the 1st arg cos it's the program name.
    let mut start = 1;

    if args.len() > start {
        if args[start].starts_with("_") {
            start = 2;
        }

        // add in any command line args we where passed
        for i in start..args.len() {
            cmd.arg(&args[i]);
        }
    }

    choice = 1;
    for name in ks {
        if choice == chosen {
            let empty_string = "".to_owned();
            let services = results.get(*name).unwrap();
//            println!("{:?}", services[0]);
            let host = match service_type {
                "_ssh._tcp" => name.to_string(),
                "_http._tcp" => {
                    let sz = & services[0];
                    println!("{:?}", sz);
                    let txt = match sz.txt {
                        Some(ref thing) => thing,
                        None => &empty_string
                    };
                    let found = txt.find("=").unwrap_or(0) + 1;
                    let txt = txt.to_string();
                    // chop "path= off the beginning
                    let (_, txt) = txt.split_at(found);
                    // and " off of the end
                    let (txt, _) = txt.split_at(txt.len() - 1);
                    let host_name = match sz.host_name {
                        Some(ref thing) => thing,
                        None => &empty_string
                    };
                    let txt = format!("http://{}:{}{}", host_name, sz.port, txt); //.txt.unwrap_or(empty_string));
                    txt
                },
                _ => name.to_string()
            };
            cmd.arg(&host);
            chose = true;
            break;
        }
        choice += 1;
    }

    if chose {
        let status = cmd.status().unwrap_or_else(|e| panic!("failed to execute process: {}", e));
        return status.code().unwrap();
    } else {
        println!("Didn't find that choice: {}", choice);
        return -1;
    }
}
